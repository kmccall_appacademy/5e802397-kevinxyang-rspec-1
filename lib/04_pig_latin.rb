def translate(words)
  words_to_translate = words.split(' ')
  translated_words = []

  words_to_translate.each do |word|
    translated_words << pig_latinfy(word)
  end

  translated_words.join(' ')
end

def pig_latinfy(word)
  latin_word = []

  vowels = %w(a e i o u)
  broken_word = word.chars

  if contains_qu?(broken_word)
    broken_word = qu_translate(broken_word)
  else
    i = 0
    while i < broken_word.length
      break if vowels.include?(broken_word[i])
      broken_word.push(broken_word[i])
      broken_word.delete_at(i)
    end
  end

  broken_word << "ay"
  broken_word.join
end

def contains_qu?(word_into_leters)
  return true if word_into_leters.join.include?("qu")
  false
end

def qu_translate(word_into_leters)
  if word_into_leters[0..1].join.include?("qu")
    word_into_leters = qu_mover(word_into_leters)
  elsif word_into_leters[1..2].join.include?("qu")
    word_into_leters = qu_mover(word_into_leters, 0, 2)
  end
end

def qu_mover(word_into_leters, x = 0, y = 1)
  word_into_leters.push(word_into_leters[x..y])
  (y + 1).times { word_into_leters.delete_at(x) }
  word_into_leters
end
