def add(n, i)
  n + i
end

def sum(numbers)
  return 0 if numbers == []
  numbers.reduce(:+)
end

def subtract(n, i)
  n - i
end
