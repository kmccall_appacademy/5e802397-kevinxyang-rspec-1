def echo(word)
  p word
end

def shout(word)
  upper_word = word.upcase
  p upper_word
end

def repeat(word, repeats_value = 2)
  words = []

  i = 0
  while i < repeats_value
    words << word
    i += 1
  end

  words.join(' ')
end

def start_of_word(word, number_of_letters)
  s = 'apple'

  word_array = word.chars
  wanted_letters = word_array.take(number_of_letters)
  wanted_letters.join('')
end

def first_word(word)
  word.split.take(1).join
end

def titleize(title)
  words = title.split(' ')
  word_holder = words[1...words.length]
  skipped_words = %w(and the on over)
  titleized_words = [uppercase_first_letter(words[0])]

  word_holder.each do |word|
    titleized_words << uppercase_first_letter(word) unless skipped_words.include?(word)
    titleized_words << word if skipped_words.include?(word)
  end

  titleized_words.join(' ')
end

def uppercase_first_letter(word)
  letters = word.chars
  letters[0].upcase!
  letters.join
end
