def ftoc(fahrenheit_tempature)
  conversion = (fahrenheit_tempature - 32) / 1.8
  conversion.ceil
end

def ctof(celsius_tempature)
  (celsius_tempature * 1.8) + 32
end
